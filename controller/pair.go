package controller

import (
	"encoding/json"
	"net/http"
	"time"
	"wegecan/dto"
	"wegecan/lib"
	"wegecan/service/api"

	"github.com/gin-gonic/gin"
)

type Pair struct{}

var sapi = api.API{}

//New 建構式
func (p Pair) New() *Pair {
	return &p
}

//GetMerketPairs 取得媒合資料
func (p *Pair) GetMerketPairs(c *gin.Context) {
	db := lib.InfluxDB{}.New()
	query := `
		from(bucket: "interview")
		|>	range(start: -24h)
		|>	filter(fn: (r) => r._measurement == "market_pair")`
	result := db.Query(query)
	c.JSON(http.StatusOK, gin.H{
		"status":  200000,
		"message": result,
	})
	return
}

//AddMerketPairs 寫入媒合資料
func (p *Pair) AddMerketPairs(c *gin.Context) {
	api := sapi.New("coinMarketCap")
	result := api.GetMarketPairs()
	if result.Status != success {
		dtoRO := ro.Build(400001, result.Message)
		c.JSON(http.StatusBadRequest, dtoRO)
		return
	}
	data := []byte(result.Message)
	marketPair := &dto.CMCMarketPair{}
	json.Unmarshal(data, marketPair)
	if marketPair.Status.ErrorCode != fail {
		dtoRO := ro.Build(400002, marketPair.Status.ErrorMessage)
		c.JSON(http.StatusBadRequest, dtoRO)
		return
	}
	db := lib.InfluxDB{}.New()
	for _, value := range marketPair.Data {
		for _, row := range value.MarketPairs {
			item := make(map[string]interface{})
			item["market_pair"] = row.MarketPair
			item["category"] = row.Category
			item["fee_type"] = row.FeeType
			item["price"] = row.Quote.Price
			t := time.Now()
			db.Add("market_pair", item, t)
		}
	}
	c.JSON(http.StatusOK, gin.H{
		"status":  200000,
		"message": gin.H{},
	})
	return
}

package controller

import (
	"net/http"
	"wegecan/service"

	"github.com/gin-gonic/gin"
)

//Users 使用者相關資料結構
type User struct{}

type login struct {
	Account  string `json:"account"`
	Password string `json:"password"`
}

var suser = service.User{}.New()

//New 建構式
func (u User) New() *User {
	return &u
}

//Verify 驗証
func (u *User) Verify(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"status":  200000,
		"message": gin.H{},
	})
	return
}

//Login 登入
func (u *User) Login(c *gin.Context) {
	var params login
	if err := c.BindJSON(&params); err != nil {
		dtoRO := ro.Build(400001, err.Error())
		c.JSON(http.StatusBadRequest, dtoRO)
		return
	}

	if params.Account != "peter" || params.Password != "12345678" {
		dtoRO := ro.Build(400002, "User not found")
		c.JSON(http.StatusBadRequest, dtoRO)
		return
	}

	dtoRO := suser.GetAccessToken()
	if dtoRO.Status == fail {
		dtoRO := ro.Build(400003, dtoRO.Message)
		c.JSON(http.StatusBadRequest, dtoRO)
		return
	}
	c.Header("Access-Token", dtoRO.Message)
	c.JSON(http.StatusOK, gin.H{
		"status":  200000,
		"message": gin.H{},
	})
	return
}

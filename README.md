# WegeCan
面試用作業題

## 資料夾結構
controller Restful API呼叫用控制器

dev_env Docker Composer 相關設定

dto 資料傳輸物件

lib 常用函式庫

service Restful API 服務

test PostMan測試檔

.env 系統設定

main.go 主程式


## 程式操作流程
1. 將env.swp更名為.env
2. 進入dev_env資料夾，啟用docker compose
3. 進入http://10.1.0.13:8086取得influxdb的token
4. 將influxdb的token寫入.env，並視情況修改內容
5. 運行程式碼，go run main.go
6. 將test資料夾中的設定匯入PostMan，可以進行測試


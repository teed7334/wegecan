package lib

import (
	"context"
	"os"
	"time"

	client "github.com/influxdata/influxdb-client-go/v2"
	_ "github.com/joho/godotenv/autoload"
)

//Influxdb 物件參數
type InfluxDB struct{}

//New 建構式
func (ib InfluxDB) New() *InfluxDB {
	return &ib
}

//Open 開啟資料庫
func (ib *InfluxDB) open() client.Client {
	cli := client.NewClient(os.Getenv("influxdb_host"), os.Getenv("influxdb_token"))
	return cli
}

//Add 寫入資料庫
func (ib *InfluxDB) Add(table string, fields map[string]interface{}, t time.Time) {
	cli := ib.open()
	p := client.NewPointWithMeasurement(table)
	for key, value := range fields {
		p.AddField(key, value)
	}
	p.SetTime(t)
	writeAPI := cli.WriteAPIBlocking(os.Getenv("influxdb_org"), os.Getenv("influxdb_bucket"))
	writeAPI.WritePoint(context.Background(), p)
	cli.Close()
}

func (ib *InfluxDB) Query(query string) []map[string]interface{} {
	cli := ib.open()
	q := cli.QueryAPI(os.Getenv("influxdb_org"))
	result, err := q.Query(context.Background(), query)
	if err != nil {
		return nil
	}
	var items []map[string]interface{}
	for result.Next() {
		item := result.Record().Values()
		items = append(items, item)
	}
	return items
}

package lib

import (
	"io/ioutil"
	"net/http"
	"net/url"
)

type Curl struct{}

//New 建構式
func (c Curl) New() *Curl {
	return &c
}

//GET 透過HTTP GET取得資料
func (c *Curl) Get(apiPath string, query, headers map[string]string) []byte {
	client := &http.Client{}
	req, err := http.NewRequest("GET", apiPath, nil)
	if err != nil {
		print(err.Error())
		return nil
	}
	req.Header.Set("Accepts", "application/json")
	for key, value := range headers {
		req.Header.Set(key, value)
	}
	q := url.Values{}
	for key, value := range query {
		q.Add(key, value)
	}
	req.URL.RawQuery = q.Encode()
	resp, err := client.Do(req)
	if err != nil {
		print(err.Error())
		return nil
	}
	body, _ := ioutil.ReadAll(resp.Body)
	return body
}

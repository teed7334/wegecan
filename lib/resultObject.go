package lib

import (
	"wegecan/dto"
)

//ResultObject 物件參數
type ResultObject struct{}

//New 建構式
func (ro ResultObject) New() *ResultObject {
	return &ro
}

//Build 製作成RO物件
func (ro *ResultObject) Build(status int, message string) *dto.ResultObject {
	dtoRO := &dto.ResultObject{}
	dtoRO.Status = status
	dtoRO.Message = message
	return dtoRO
}

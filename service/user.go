package service

import (
	"os"
	"wegecan/dto"
	"wegecan/lib"

	"github.com/dgrijalva/jwt-go"
)

type User struct{}

type Token struct {
	UserId uint
	jwt.StandardClaims
}

//New 建構式
func (u User) New() *User {
	return &u
}

const success, fail = 1, 0

var ro = lib.ResultObject{}.New()

//Verify 驗証
func (u *User) Verify(accessToken string) *dto.ResultObject {
	jwtToken, err := jwt.ParseWithClaims(accessToken, &jwt.StandardClaims{}, func(token *jwt.Token) (i interface{}, e error) {
		return []byte(os.Getenv("salt")), nil
	})
	if err == nil && jwtToken != nil {
		if _, ok := jwtToken.Claims.(*jwt.StandardClaims); ok && jwtToken.Valid {
			dtoRO := ro.Build(success, accessToken)
			return dtoRO
		}
	}
	dtoRO := ro.Build(fail, err.Error())
	return dtoRO
}

//GetAccessToken 取得存取權限
func (u *User) GetAccessToken() *dto.ResultObject {
	tk := &Token{UserId: 1}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, err := token.SignedString([]byte(os.Getenv("salt")))
	if err != nil {
		dtoRO := ro.Build(fail, err.Error())
		return dtoRO
	}
	dtoRO := ro.Build(success, tokenString)
	return dtoRO
}

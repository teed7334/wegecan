package api

import (
	"os"
	"wegecan/dto"
	"wegecan/lib"
)

type CoinMarketCap struct{}

//New 建構式
func (c CoinMarketCap) New() *CoinMarketCap {
	return &c
}

//GetMarketPairs 取得成交記錄
func (cmc *CoinMarketCap) GetMarketPairs() *dto.ResultObject {
	path := os.Getenv("coinmarketcap_url") + "/v1/exchange/market-pairs/latest"
	query := make(map[string]string)
	query["slug"] = "defi"
	query["limit"] = "300"
	query["category"] = "all"
	query["fee_type"] = "all"
	headers := make(map[string]string)
	headers["X-CMC_PRO_API_KEY"] = os.Getenv("coinmarketcap_token")
	curl := lib.Curl{}.New()
	resq := curl.Get(path, query, headers)
	if resq == nil {
		dtoRO := ro.Build(fail, "Can't get CoinMarketCap API Data")
		return dtoRO
	}
	body := string(resq)
	dtoRO := ro.Build(success, body)
	return dtoRO
}

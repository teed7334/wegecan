package api

import (
	"wegecan/dto"
	"wegecan/lib"

	_ "github.com/joho/godotenv/autoload"
)

var ro = lib.ResultObject{}.New()

const success, fail = 1, 0

type IAPI interface {
	GetMarketPairs() *dto.ResultObject
}

type API struct{}

func (a API) New(category string) IAPI {
	switch category {
	case "coinMarketCap":
		api := CoinMarketCap{}.New()
		return api
	default:
		panic("Unknown api category")
	}
}

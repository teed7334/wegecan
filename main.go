package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"wegecan/controller"
	"wegecan/lib"
	"wegecan/service"

	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv/autoload"
)

var ro = lib.ResultObject{}.New()
var user = controller.User{}.New()
var pair = controller.Pair{}.New()
var suser = service.User{}.New()

const success, fail = 1, 0

func oauth(c *gin.Context) {
	header := c.Request.Header.Get("Authorization")
	arr := strings.Split(header, "Bearer ")
	if len(arr) != 2 {
		dtoRO := ro.Build(400001, "User not found")
		c.Abort()
		c.JSON(http.StatusBadRequest, dtoRO)
		return
	}
	accessToken := arr[1]
	dtoRO := suser.Verify(accessToken)
	if dtoRO.Status == fail {
		dtoRO = ro.Build(400002, dtoRO.Message)
		c.Abort()
		c.JSON(http.StatusBadRequest, dtoRO)
		return
	}
	c.Next()
}

//main 主程式
func main() {
	port := fmt.Sprintf(":%s", os.Getenv("port"))
	router := gin.Default()
	v1 := router.Group("/api/v1")
	{
		v1.POST("/login", user.Login)
	}
	v1.Use(oauth)
	{
		v1.POST("/verify", user.Verify)
		v1.GET("/market_pairs", pair.GetMerketPairs)
		v1.POST("/market_pairs", pair.AddMerketPairs)
	}
	router.Run(port)
}

package dto

//CMCMarketPair CoinMarketCap Market Pair相關資料結構
type CMCMarketPair struct {
	Status status          `json:"status"`
	Data   map[string]data `json:"data"`
}

type status struct {
	Status       string `json:"status"`
	ErrorCode    int    `json:"error_code"`
	ErrorMessage string `json:"error_message"`
}

type data struct {
	ID             uint         `json:"id"`
	Name           string       `json:"name"`
	Slug           string       `json:"slug"`
	NumMarketPairs uint         `json:"num_market_pairs"`
	MarketPairs    []marketPair `json:"market_pairs"`
}

type marketPair struct {
	MarketID        uint             `json:"market_id"`
	MarketPair      string           `json:"market_pair"`
	Category        string           `json:"category"`
	FeeType         string           `json:"fee_type"`
	MarketPairBase  []marketPairBase `json:"market_pair_base"`
	MarketPairQuote []marketPairBase `json:"market_pair_quote"`
	Quote           exchangeReported `json:"quote"`
}

type marketPairBase struct {
	CurrencyID     uint   `json:"currency_id"`
	CurrencySymbol string `json:"currency_symbol"`
	ExchangeSymbol string `json:"exchange_symbol"`
	CurrencyType   string `json:"currency_type"`
}

type exchangeReported struct {
	Price          float64 `json:"price"`
	Volume24hBase  float64 `json:"volume_24h_base"`
	Volume24hQuote float64 `json:"volume_24h_quote"`
	LastUpdated    string  `json:"last_updated"`
}
